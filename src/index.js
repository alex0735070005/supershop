class Car {

    constructor(y) {
        this.engine = y ? y :'mt-348';
        this.steering = 'sport';
    }
    
    start(){
        console.log('started');
    }

    stop(){
        console.log('stopped');
    }

    openDoor(side) {
        console.log(`opened ${side}`);
    }
}

class BMW {
    constructor () {
        this.model = 'i8';
    }

    openDoor(side) {
        console.log(`opened BMV ${side}`);
    }
}

class Audi extends Car {
    constructor (setting) {
        super(setting.engine);
        this.model = setting.model || false;
    }
}

var audi = new Audi({
    //model:'A7'
});

console.log(audi.engine, audi.model);

//audi.openDoor('left');

//var bmw = new BMW();

//bmw.openDoor('right');